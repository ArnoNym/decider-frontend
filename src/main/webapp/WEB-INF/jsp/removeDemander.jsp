<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>remove demander</title>
</head>
<body>

<sf:form modelAttribute="idNameElementList" method="get">
    <input type="hidden" name="decisionId" value="${decisionId}" />

    <input type="submit" value="cancel" formaction="/openDecision" formnovalidate />

    <br>
    <br>

    <table border="1">
        <tr>
            <th>demander name</th>
            <th>remove?</th>
        </tr>

        <c:forEach varStatus="i" var="idNameElement" items="${idNameElementList.elementList}">
            <tr>
                <td>${idNameElement.name}</td>
                <td>
                    <input name="elementList[${i.index}].id" type="checkbox" value="${idNameElement.id}" />
                    <!-- <input type="hidden" name="_elementList[${i.index}].id" value="${null}"/> -->
                </td>
            </tr>
        </c:forEach>
    </table>

    <br>

    <input type="submit" value="remove (cant be reverted)" formaction="/removeDemander">
</sf:form>

</body>
</html>
