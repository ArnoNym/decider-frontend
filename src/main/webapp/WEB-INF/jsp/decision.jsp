<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html lang="en">
<head>
    <title>Decision</title>
</head>
<body>

<br>
Welcome to your decision '${decision.name}'<br>
<br>
To open it later remember this decisions ID: '${decision.id}'<br>
<br>
<br>

<table>
    <tr>
        <td>
            <sf:form action="newOffer">
                <input type="hidden" name="decisionId" value="${decision.id}">
                <input type="submit" value="New Offer">
            </sf:form>
        </td>
        <c:if test="${!decision.offerList.isEmpty()}">
            <td>
                <sf:form action="openRemoveOffer">
                    <input type="hidden" name="decisionId" value="${decision.id}">
                    <input type="submit" value="Remove Offer">
                </sf:form>
            </td>
        </c:if>
    </tr>
    <tr>
        <td>
            <sf:form action="newDemander">
                <input type="hidden" name="decisionId" value="${decision.id}">
                <!-- TODO Checkbox mit Erlaube zurueck zur Decision zu gehen -->
                <input type="submit" value="New Demander">
            </sf:form>
        </td>
        <c:if test="${!decision.demanderList.isEmpty()}">
            <td>
                <sf:form action="openRemoveDemander">
                    <input type="hidden" name="decisionId" value="${decision.id}">
                    <input type="submit" value="Remove Demander">
                </sf:form>
            </td>
        </c:if>
    </tr>
</table>

<br>

<c:if test="${!decision.demanderList.isEmpty() && !decision.offerList.isEmpty()}">
    <sf:form action="/choices" method="get">
        <input type="hidden" name="decisionId" value="${decision.id}">
        <input type="submit" value="Calculate Choices">
    </sf:form>
</c:if>
<c:if test="${!decision.demanderList.isEmpty() || !decision.offerList.isEmpty()}">
    <br>
    <sf:form modelAttribute="decision" action="/saveDecision" method="get">

        <br>
        <br>

        <sf:hidden path="id" /> <!-- TODO Alles in Felderen zum beareiten freigeben -->
        <sf:hidden path="name" />
        <sf:hidden path="minGroupCount" />
        <sf:hidden path="maxGroupCount" />
        <sf:hidden path="note" />

        <table border="1">
            <tr>
                <th colspan="5">
                    <input type="submit" value="save changes">
                </th>
                <c:forEach varStatus="i" var="pack" items="${decision.demanderList}">
                    <input type="hidden" name="demanderList[${i.index}].id" value="${pack.id}" />
                    <input type="hidden" name="demanderList[${i.index}].decisionId" value="${pack.decisionId}" />

                    <th><input type="text" name="demanderList[${i.index}].name" value="${pack.name}" /></th>
                </c:forEach>
            </tr>

            <tr>
                <th>name</th>
                <th>available</th>
                <th>min group size</th>
                <th>max group size</th>
                <th>has to be included</th>

                <c:forEach varStatus="i" var="pack" items="${decision.demanderList}">
                    <td>
                        <input name="demanderList[${i.index}].hasToBeIncluded" type="checkbox" value="true"
                            <c:if test="${pack.hasToBeIncluded}"> checked </c:if>/>
                        <input type="hidden" name="_demanderList[${i.index}].hasToBeIncluded" value="on"/>
                    </td>
                </c:forEach>
            </tr>

            <c:forEach varStatus="i" var="offer" items="${decision.offerList}">
                <tr>
                <input type="hidden" name="offerList[${i.index}].id" value="${offer.id}" />
                <input type="hidden" name="offerList[${i.index}].decisionId" value="${offer.decisionId}" />

                <td><input type="text" name="offerList[${i.index}].name" value="${offer.name}" /></td>
                <td><input type="number" name="offerList[${i.index}].available" value="${offer.available}" /></td>
                <td><input type="number" name="offerList[${i.index}].minGroupSize" value="${offer.minGroupSize}" /></td>
                <td><input type="number" name="offerList[${i.index}].maxGroupSize" value="${offer.maxGroupSize}" /></td>
                <td>
                    <input name="offerList[${i.index}].hasToBeIncluded" type="checkbox" value="true"
                            <c:if test="${offer.hasToBeIncluded}"> checked </c:if>/>
                    <input type="hidden" name="_offerList[${i.index}].hasToBeIncluded" value="on"/>
                </td>

                <c:forEach varStatus="j" var="pack" items="${decision.demanderList}">
                    <input type="hidden" name="demanderList[${j.index}].opinionList[${i.index}].demanderId"
                           value="${pack.opinionList.get(i.index).demanderId}" />
                    <input type="hidden" name="demanderList[${j.index}].opinionList[${i.index}].offerId"
                           value="${pack.opinionList.get(i.index).offerId}" />
                    <input type="hidden" name="demanderList[${j.index}].opinionList[${i.index}].note"
                           value="${pack.opinionList.get(i.index).note}" />

                    <td><input type="number" name="demanderList[${j.index}].opinionList[${i.index}].value"
                               value="${pack.opinionList.get(i.index).value}" /></td>
                </c:forEach>
                </tr>
            </c:forEach>

        </table>
    </sf:form>
</c:if>

</body>
</html>