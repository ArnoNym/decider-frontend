<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html lang="en">
<head>
    <title>Choices</title>
</head>
<body>

<sf:form action="/openDecision">
    <input type="hidden" name="decisionId" value="${decisionId}">
    <input type="submit" value="back" /><br>
</sf:form>
<br>
<c:forEach var="choice" items="${choiceList}">
    Value: ${choice.calcUtility()}<br>
    <c:forEach var="pack" items="${choice.packList}">
        ${pack.offer.name()} ->
        <c:set var="demanderListSize" value="${pack.demanderList.size()}" />
        <c:forEach varStatus="i" var="demander" items="${pack.demanderList}">
            ${demander.name()}
            <c:if test="${i.index < (demanderListSize - 1)}">
                //
            </c:if>
        </c:forEach>
        <br>
    </c:forEach>
    <br>
</c:forEach>

</body>
</html>
