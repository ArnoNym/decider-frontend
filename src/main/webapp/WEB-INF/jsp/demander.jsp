<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Demander</title>
</head>
<body>

<sf:form modelAttribute="demander" method="get">
    <sf:hidden path="id" />
    <sf:hidden path="decisionId" />

    <input type="submit" value="back" formaction="/openDecision" formnovalidate />

    <table>

        <tr>
            <td>Name</td>
            <td><sf:input path="name" required="true" /></td>
        </tr>

        <tr>
            <td>Insists on group</td>
            <td><sf:checkbox path="hasToBeIncluded" /></td>
        </tr>

        <br>
        <c:forEach varStatus="i" var="opinion" items="${demander.opinionList}">
            <tr>
                <input type="hidden" name="opinionList[${i.index}].demanderId" value="${opinion.demanderId}" />
                <input type="hidden" name="opinionList[${i.index}].offerId" value="${opinion.offerId}" />
                <input type="hidden" name="opinionList[${i.index}].note" value="${opinion.note}" />

                <td>${opinion.offer.name}</td>
                <td><input type="number" name="opinionList[${i.index}].value" value="${opinion.value}" /></td>
            </tr>
        </c:forEach>

    </table>

    <input type="submit" value="save demander" formaction="/saveNewDemander">
    <input type="submit" value="save demander, go to decision" formaction="/saveNewDemanderGoToDecider">
</sf:form>

</body>
</html>
