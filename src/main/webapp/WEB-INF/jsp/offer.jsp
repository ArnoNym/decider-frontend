<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html lang="en">
<head>
    <title>Offer</title>
</head>
<body>

<sf:form modelAttribute="offer" method="get">
    <sf:hidden path="id" />
    <sf:hidden path="decisionId" />

    <input type="submit" value="back" formaction="/openDecision" formnovalidate />

    <table>
        <tr>
            <td>Name</td>
            <td><sf:input path="name" required="true" /></td>
        </tr>
        <tr>
            <td>Has to be included</td>
            <td><sf:checkbox path="hasToBeIncluded" /></td>
        </tr>
        <tr>
            <td>Available</td>
            <td><sf:input path="available" required="true" /></td>
        </tr>
        <tr>
            <td>Min group size</td>
            <td><sf:input path="minGroupSize" required="true" /></td>
        </tr>
        <tr>
            <td>Max group size</td>
            <td><input name="maxGroupSize" value="${offer.maxGroupSize}" placeholder="Infinity by default" /></td>
        </tr>

        <br>
        <c:forEach varStatus="i" var="opinion" items="${demander.opinionList}">
            <tr>
                <input type="hidden" name="opinionList[${i.index}].demanderId" value="${opinion.demanderId}" />
                <input type="hidden" name="opinionList[${i.index}].offerId" value="${opinion.offerId}" />
                <input type="hidden" name="opinionList[${i.index}].note" value="${opinion.note}" />

                <td>${opinion.offer.name}</td>
                <td><input type="number" name="opinionList[${i.index}].value" value="${opinion.value}" /></td>
            </tr>
        </c:forEach>

    </table>

    <br>

    <input type="submit" value="save offer" formaction="/saveNewOffer">
    <input type="submit" value="save offer, go to decision" formaction="/saveNewOfferGoToDecider">
</sf:form>

</body>
</html>