package an.deciderweb;

import an.deciderweb.model.Decision;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class DeciderwebApplication {
    //public static final EntityManagerFactory emf;

    public static void main(String[] args) {
        SpringApplication.run(DeciderwebApplication.class, args);
    }

    /*
    static {
        Map<String, String> properties = new HashMap<>();
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.format_sql", "true");
        emf = Persistence.createEntityManagerFactory("jpa-query-types", properties);
    }

    public static EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    */

    private static void loadTestData() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Student_details");
        EntityManager em=emf.createEntityManager();

        em.getTransaction().begin();

        Decision decision = new Decision();
        decision.setName("BrettAG_Essen");
        em.persist(decision);

        em.getTransaction().commit();

        emf.close();
        em.close();
    }

}
