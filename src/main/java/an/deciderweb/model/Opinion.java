package an.deciderweb.model;

import an.deciderweb.model.id.OpinionId;

import javax.persistence.*;
import java.io.Serializable;

@Entity  @IdClass(OpinionId.class)
public class Opinion implements an.decider.input.Opinion, Serializable {
    @Id
    @Column(name = "demander_Id")//TODO Unterkringelnt entfernen. Oder am besten diese Namenszuweiseung komplett ueberfluessig machen
    private Integer demanderId;
    @Id
    @Column(name = "offer_Id")
    private Integer offerId;

    @ManyToOne //TODO Pruefe ob fetching type lazy eine gute Idee ist
    @MapsId("demanderId")
    private Demander demander;
    @ManyToOne
    @MapsId("offerId")
    private Offer offer;

    private Double value;
    private String note = null;

    public Opinion() {
    }

    public Integer getDemanderId() {
        return demanderId;
    }

    public void setDemanderId(Integer demanderId) {
        this.demanderId = demanderId;
    }

    public Integer getOfferId() {
        return offerId;
    }

    public void setOfferId(Integer offerId) {
        this.offerId = offerId;
    }

    public Demander getDemander() {
        return demander;
    }

    public void setDemander(Demander demander) {
        this.demander = demander;
    }

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public Integer demanderId() {
        return getDemanderId();
    }

    @Override
    public Integer offerId() {
        return getOfferId();
    }

    @Override
    public Double value() {
        return getValue();
    }
}
