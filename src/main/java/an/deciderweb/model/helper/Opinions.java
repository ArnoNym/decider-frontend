package an.deciderweb.model.helper;

import an.deciderweb.model.Opinion;

import java.util.*;

public class Opinions implements Iterable<Opinion> {
    private final Collection<Opinion> opinions;
    private final Map<Integer, Map<Integer, Double>> demanderId_offerId_opinionValue = new HashMap<>();

    public Opinions(Collection<Opinion> opinions) {
        this.opinions = opinions;

        for (Opinion opinion : opinions) {
            /*//Integer demanderId = opinion.getDemanderId();
            //Integer offerId = opinion.getOfferId();
            Double opinionValue = opinion.getValue();

            Map<Integer, Double> offerId_opinionValue = demanderId_offerId_opinionValue.get(demanderId);
            if (offerId_opinionValue == null) {
                offerId_opinionValue = new HashMap<>();
            }

            offerId_opinionValue.put(offerId, opinionValue);

            demanderId_offerId_opinionValue.put(demanderId, offerId_opinionValue);*/
        }
    }

    public Double get(Integer demanderId, Integer offerId) {
        Map<Integer, Double> offerId_opinionValue = demanderId_offerId_opinionValue.get(demanderId);
        if (offerId_opinionValue == null) {
            return null;
        }
        return offerId_opinionValue.get(offerId);
    }

    @Override
    public Iterator<Opinion> iterator() {
        return opinions.iterator();
    }
}
