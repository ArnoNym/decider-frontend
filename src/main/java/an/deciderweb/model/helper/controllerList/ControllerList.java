package an.deciderweb.model.helper.controllerList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ControllerList<O> implements Iterable<O> {
    private List<O> elementList = new ArrayList<>();

    public ControllerList() {
    }

    public void add(O o) {
        elementList.add(o);
    }

    @Override
    public Iterator<O> iterator() {
        return elementList.iterator();
    }

    public List<O> getElementList() {
        return elementList;
    }

    public void setElementList(List<O> elementList) {
        this.elementList = elementList;
    }
}
