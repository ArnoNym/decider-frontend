package an.deciderweb.model.helper;

import an.deciderweb.model.helper.controllerList.ControllerList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class IdNameElementList implements Iterable<IdNameElement> {
    private List<IdNameElement> elementList = new ArrayList<>();

    public IdNameElementList() {
    }

    public void add(IdNameElement o) {
        elementList.add(o);
    }

    @Override
    public Iterator<IdNameElement> iterator() {
        return elementList.iterator();
    }

    public List<IdNameElement> getElementList() {
        return elementList;
    }

    public void setElementList(List<IdNameElement> elementList) {
        this.elementList = elementList;
    }
}
