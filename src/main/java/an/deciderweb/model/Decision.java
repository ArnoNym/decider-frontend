package an.deciderweb.model;

import javax.persistence.*;
import java.util.*;

@Entity
public class Decision implements an.decider.input.Decision {
    @Id
    private Integer id;

    @Column(nullable = false)
    private String name;

    private String password = null;

    @Column(nullable = false)
    private int minGroupCount = 1;

    private Integer maxGroupCount = null;

    private String note = null;

    @OneToMany(mappedBy = "decision", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Offer> offerList = new ArrayList<>();

    @OneToMany(mappedBy = "decision", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Demander> demanderList = new ArrayList<>();

    public Decision() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getMinGroupCount() {
        return minGroupCount;
    }

    public void setMinGroupCount(int minGroupCount) {
        this.minGroupCount = minGroupCount;
    }

    public Integer getMaxGroupCount() {
        return maxGroupCount;
    }

    public void setMaxGroupCount(Integer maxGroupCount) {
        this.maxGroupCount = maxGroupCount;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<Offer> getOfferList() {
        return offerList;
    }

    public void setOfferList(List<Offer> offerList) {
        this.offerList = offerList;
    }

    public List<Demander> getDemanderList() {
        return demanderList;
    }

    public void setDemanderList(List<Demander> demanderList) {
        this.demanderList = demanderList;
    }

    @Override
    public String name() {
        return getName();
    }

    @Override
    public Integer minGroupCount() {
        return getMinGroupCount();
    }

    @Override
    public Optional<Integer> maxGroupCount() {
        return Optional.ofNullable(getMaxGroupCount());
    }

    @Override
    public Collection<Offer> offers() {
        return getOfferList();
    }

    @Override
    public Collection<Demander> demanders() {
        return getDemanderList();
    }
}
