package an.deciderweb.model;

public abstract class Model<ID> {
    public abstract ID getId();
}
