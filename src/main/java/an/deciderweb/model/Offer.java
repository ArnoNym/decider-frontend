package an.deciderweb.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Entity
public class Offer implements an.decider.input.Offer {
    @Id
    private Integer id;

    @Column(name = "decision_Id")
    private Integer decisionId;
    @ManyToOne
    @MapsId("decision_Id")
    private Decision decision;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private boolean hasToBeIncluded = false;

    @Column(nullable = false)
    private int available = 1;

    @Column(nullable = false)
    private int minGroupSize = 1;

    private Integer maxGroupSize = null;

    private String note = null;

    @OneToMany(mappedBy = "offer", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Opinion> opinionList = new ArrayList<>();

    public Offer() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDecisionId() {
        return decisionId;
    }

    public void setDecisionId(Integer decisionId) {
        this.decisionId = decisionId;
    }

    public Decision getDecision() {
        return decision;
    }

    public void setDecision(Decision decision) {
        this.decision = decision;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHasToBeIncluded() {
        return hasToBeIncluded;
    }

    public void setHasToBeIncluded(boolean hasToBeIncluded) {
        this.hasToBeIncluded = hasToBeIncluded;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public int getMinGroupSize() {
        return minGroupSize;
    }

    public void setMinGroupSize(int minGroupSize) {
        this.minGroupSize = minGroupSize;
    }

    public Integer getMaxGroupSize() {
        return maxGroupSize;
    }

    public void setMaxGroupSize(Integer maxGroupSize) {
        this.maxGroupSize = maxGroupSize;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<Opinion> getOpinionList() {
        return opinionList;
    }

    public void setOpinionList(List<Opinion> opinionList) {
        this.opinionList = opinionList;
    }

    @Override
    public Integer id() {
        return getId();
    }

    @Override
    public String name() {
        return getName();
    }

    @Override
    public Boolean hasToBeIncluded() {
        return isHasToBeIncluded();
    }

    @Override
    public Integer available() {
        return getAvailable();
    }

    @Override
    public Integer minGroupSize() {
        return getMinGroupSize();
    }

    @Override
    public Optional<Integer> maxGroupSize() {
        return Optional.ofNullable(getMaxGroupSize());
    }
}
