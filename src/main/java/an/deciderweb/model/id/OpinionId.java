package an.deciderweb.model.id;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class OpinionId implements Serializable {
    private Integer demanderId;
    private Integer offerId;

    public OpinionId() {
    }

    public Integer getDemanderId() {
        return demanderId;
    }

    public void setDemanderId(Integer demanderId) {
        this.demanderId = demanderId;
    }

    public Integer getOfferId() {
        return offerId;
    }

    public void setOfferId(Integer offerId) {
        this.offerId = offerId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(demanderId, offerId);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof OpinionId)) {
            return false;
        }
        OpinionId opinionId = (OpinionId) obj;
        return demanderId != null && demanderId.equals(opinionId.demanderId)
                && offerId != null && offerId.equals(opinionId.offerId);
    }
}
