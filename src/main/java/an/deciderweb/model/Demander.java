package an.deciderweb.model;

import an.deciderweb.Constants;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.*;

@Entity
public class Demander implements an.decider.input.Demander {
    @Id
    private Integer id;

    @Column(name = "decision_Id")
    private Integer decisionId;
    @ManyToOne
    @MapsId("decision_Id")
    private Decision decision;

    @Column(nullable = false)
    private String name;

    private boolean hasToBeIncluded = true;

    @OneToMany(mappedBy = "demander", cascade = CascadeType.ALL, orphanRemoval = true)//todo bringt das cascade und orphaRemoval etwas?? Sollte das da stehen? orphanRemoval = true
    private List<Opinion> opinionList = new ArrayList<>();

    public Demander() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDecisionId() {
        return decisionId;
    }

    public void setDecisionId(Integer decisionId) {
        this.decisionId = decisionId;
    }

    public Decision getDecision() {
        return decision;
    }

    public void setDecision(Decision decision) {
        this.decision = decision;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHasToBeIncluded() {
        return hasToBeIncluded;
    }

    public void setHasToBeIncluded(boolean hasToBeIncluded) {
        this.hasToBeIncluded = hasToBeIncluded;
    }

    public List<Opinion> getOpinionList() {
        return opinionList;
    }

    public void setOpinionList(List<Opinion> opinionList) {
        this.opinionList = opinionList;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("id=").append(id).append("\n");
        sb.append("decisionId=").append(decisionId).append("\n");
        sb.append("name=").append(name).append("\n");
        sb.append("hasToBeIncluded=").append(hasToBeIncluded).append("\n");
        int i = 0;
        for (Opinion opinion : opinionList) {
            sb.append("opinion_").append(i).append("_demanderId=").append(opinion.getDemanderId()).append(", ");
            sb.append("opinion_").append(i).append("_offerId=").append(opinion.getOfferId()).append(", ");
            sb.append("opinion_").append(i).append("_value=").append(opinion.getValue()).append(", ");
            sb.append("opinion_").append(i).append("_note=").append(opinion.getNote()).append("\n");
            i++;
        }
        return sb.toString();
    }

    @Override
    public Integer id() {
        return getId();
    }

    @Override
    public String name() {
        return getName();
    }

    @Override
    public Boolean hasToBeIncluded() {
        return isHasToBeIncluded();
    }

    @Override
    public Collection<Opinion> opinions() {
        return getOpinionList();
    }
}
