package an.deciderweb.controller;

import an.deciderweb.dao.DemanderDao;
import an.deciderweb.dao.OfferDao;
import an.deciderweb.dao.OpinionDao;
import an.deciderweb.model.Demander;
import an.deciderweb.model.Offer;
import an.deciderweb.model.Opinion;
import an.deciderweb.model.helper.IdNameElement;
import an.deciderweb.model.helper.IdNameElementList;
import an.deciderweb.model.helper.controllerList.NameList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.LinkedList;
import java.util.List;

@Controller
public class OfferController {
    @Autowired
    private OfferDao offerDao;
    @Autowired
    private OpinionDao opinionDao;
    @Autowired
    private DemanderDao demanderDao;
    @Autowired
    private DecisionController decisionController;

    @RequestMapping("/newOffer")
    public ModelAndView newOffer(@RequestParam Integer decisionId, NameList addedOfferNameList) {
        // offer

        Offer offer = new Offer();

        offer.setDecisionId(decisionId);

        // mv

        ModelAndView mv = new ModelAndView("offer");
        mv.addObject("addedOfferNameList", addedOfferNameList);
        mv.addObject("offer", offer);
        return mv;
    }

    @RequestMapping("/saveNewOffer")
    public ModelAndView saveNewOffer(Offer offer, NameList addedDemanderNameList) {
        saveNewOffer(offer);
        return newOffer(offer.getDecisionId(), addedDemanderNameList);
    }

    @RequestMapping("/saveNewOfferGoToDecider")
    public ModelAndView saveNewOfferGoToDecider(Offer offer) {
        saveNewOffer(offer);
        return decisionController.decision(offer.getDecisionId());
    }

    private void saveNewOffer(Offer offer) {
        final int offerId;
        final Integer maxId = offerDao.findMaxId();
        if (maxId == null) {
            offerId = 0;
        } else {
            offerId = maxId + 1;
        }
        offer.setId(offerId);

        offerDao.save(offer);

        List<Opinion> opinionList = new LinkedList<>();
        List<Integer> demanderIdList = demanderDao.findAllId();
        for (Integer demanderId : demanderIdList) {
            Opinion opinion = new Opinion();
            opinion.setDemanderId(demanderId);
            opinion.setOfferId(offerId);
            opinionList.add(opinion);
        }
        opinionDao.saveAll(opinionList);
    }

    // REMOVE =========================================================================================================

    @RequestMapping("/openRemoveOffer")
    public ModelAndView openRemoveDemander(@RequestParam Integer decisionId) {
        IdNameElementList idNameElementList = new IdNameElementList();
        for (Offer offer : offerDao.findAllByDecisionId(decisionId)) {
            idNameElementList.add(new IdNameElement(offer.getId(), offer.getName()));
        }

        ModelAndView mv = new ModelAndView("removeOffer");
        mv.addObject("decisionId", decisionId);
        mv.addObject("idNameElementList", idNameElementList);
        return mv;
    }

    @RequestMapping("/removeOffer")
    public ModelAndView removeDemander(@RequestParam Integer decisionId, IdNameElementList idNameElementList) {
        List<Offer> offerList = new LinkedList<>();
        for (IdNameElement idNameElement : idNameElementList) {
            Offer offer = new Offer();
            offer.setId(idNameElement.getId());
            offerList.add(offer);
        }
        offerDao.deleteAll(offerList);

        return decisionController.decision(decisionId);
    }
}
