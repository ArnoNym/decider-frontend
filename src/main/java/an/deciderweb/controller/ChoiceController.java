package an.deciderweb.controller;

import an.decider.calc.Decider;
import an.decider.output.Choice;
import an.deciderweb.dao.DecisionDao;
import an.deciderweb.model.Decision;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Optional;

@Controller
public class ChoiceController {
    @Autowired
    DecisionDao decisionDao;

    @RequestMapping("/choices")
    public ModelAndView choices(@RequestParam Integer decisionId) {
        Optional<Decision> decisionOptional = decisionDao.findById(decisionId);
        if (decisionOptional.isEmpty()) {
            throw new IllegalStateException();
        }
        Decision decision = decisionOptional.get();

        // todo pruefe ob decision valid ist. Das nicht mit dem Form machen damit nicht jemand anderes Aenderungen vornimmt und dann eine alte version ausgerechnet wird

        List<Choice> choiceList = Decider.createChoiceList(decision);

        ModelAndView mv = new ModelAndView("choices");
        mv.addObject("decisionId", decisionId);
        mv.addObject("choiceList", choiceList);
        return mv;
    }
}
