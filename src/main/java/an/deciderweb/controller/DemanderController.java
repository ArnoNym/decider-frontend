package an.deciderweb.controller;

import an.deciderweb.model.helper.IdNameElement;
import an.deciderweb.model.helper.IdNameElementList;
import an.deciderweb.dao.DemanderDao;
import an.deciderweb.dao.OfferDao;
import an.deciderweb.model.*;
import an.deciderweb.model.helper.controllerList.NameList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

@Controller
public class DemanderController {
    @Autowired
    DemanderDao demanderDao;
    @Autowired
    OfferDao offerDao;
    @Autowired
    DecisionController decisionController;

    // NEW ============================================================================================================

    @RequestMapping("/newDemander")
    public ModelAndView newDemander(@RequestParam Integer decisionId, NameList addedDemanderNameList) {
                                    // todo @RequestParam boolean allowGoBackToDecision,
                                    // todo @RequestParam boolean allowAddManyDemanders) {
        // demander

        Demander demander = new Demander();

        demander.setDecisionId(decisionId);

        Iterable<Offer> offerList = offerDao.findAllByDecisionId(decisionId);
        Iterator<Offer> offerIterator = offerList.iterator();
        List<Opinion> opinionList = new LinkedList<>();
        while (offerIterator.hasNext()) {
            Opinion opinion = new Opinion();

            opinion.setDemander(demander);

            Offer offer = offerIterator.next();
            opinion.setOfferId(offer.getId());
            opinion.setOffer(offer);

            opinionList.add(opinion);
        }
        demander.setOpinionList(opinionList);

        // mv

        ModelAndView mv = new ModelAndView("demander");
        mv.addObject("addedDemanderNameList", addedDemanderNameList);
        mv.addObject("demander", demander);
        return mv;
    }

    @RequestMapping("/saveNewDemander")
    public ModelAndView saveNewDemander(Demander demander, NameList addedDemanderNameList) {
        saveNewDemander(demander);
        return newDemander(demander.getDecisionId(), addedDemanderNameList);
    }

    @RequestMapping("/saveNewDemanderGoToDecider")
    public ModelAndView saveDemanderGoToDecider(Demander demander) {
        saveNewDemander(demander);
        return decisionController.decision(demander.getDecisionId());
    }

    private void saveNewDemander(Demander demander) {
        // todo pruefen ob alle offers bewertet wurde. Ein adnerer Nutzer koennte neue hinzugefuegt haben.
        //In dem fall zurueck schicken und neue offers anzeigen (und vorhandenes speichern(?!?)

        final int demanderId;
        final Integer maxId = demanderDao.findMaxId();
        if (maxId == null) {
            demanderId = 0;
        } else {
            demanderId = maxId + 1;
        }

        demander.setId(demanderId);

        for (Opinion opinion : demander.getOpinionList()) {
            opinion.setDemanderId(demanderId);
            opinion.setDemander(demander);
        }

        System.out.println(demander.toString());

        demanderDao.save(demander);
    }

    // EDIT ===========================================================================================================

    @RequestMapping("/demander")
    public ModelAndView demander(@RequestParam Integer demanderId) {
        Demander demander;
        Optional<Demander> demanderOptional = demanderDao.findById(demanderId);
        if (demanderOptional.isEmpty()) {
            throw new IllegalArgumentException();
        }
        demander = demanderOptional.get();

        ModelAndView mv = new ModelAndView("demander");
        mv.addObject("demander", demander);
        return mv;
    }

    @RequestMapping("/saveDemander")
    public ModelAndView saveDemander(Demander demander) {
        demanderDao.save(demander);
        return demander(demander.getId());
    }

    public List<Opinion> getNewOpinions(@RequestParam Integer demanderId) {
        List<Integer> offerIdList = offerDao.findAllOfferIdsNotInOpinionForDemander(demanderId);
        if (offerIdList.isEmpty()) {
            return new LinkedList<>();
        }
        List<Opinion> opinionList = new LinkedList<>();
        for (Integer offerId : offerIdList) {
            Opinion opinion = new Opinion();
            opinion.setDemanderId(demanderId);
            opinion.setOfferId(offerId);
            opinionList.add(opinion);
        }
        return opinionList;
    }

    // REMOVE =========================================================================================================

    @RequestMapping("/openRemoveDemander")
    public ModelAndView openRemoveDemander(@RequestParam Integer decisionId) {
        IdNameElementList idNameElementList = new IdNameElementList();
        for (Demander demander : demanderDao.findAllByDecisionId(decisionId)) {
            idNameElementList.add(new IdNameElement(demander.getId(), demander.getName()));
        }

        ModelAndView mv = new ModelAndView("removeDemander");
        mv.addObject("decisionId", decisionId);
        mv.addObject("idNameElementList", idNameElementList);
        return mv;
    }

    @RequestMapping("/removeDemander")
    public ModelAndView removeDemander(@RequestParam Integer decisionId, IdNameElementList idNameElementList) {
        List<Demander> demanderList = new LinkedList<>();
        for (IdNameElement idNameElement : idNameElementList) {
            Demander demander = new Demander();
            demander.setId(idNameElement.getId());
            demanderList.add(demander);
        }
        demanderDao.deleteAll(demanderList);

        return decisionController.decision(decisionId);
    }
}
