package an.deciderweb.controller;

import an.deciderweb.dao.DecisionDao;
import an.deciderweb.dao.DemanderDao;
import an.deciderweb.dao.OfferDao;
import an.deciderweb.dao.OpinionDao;
import an.deciderweb.model.Decision;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

@Controller
public class DecisionController {
    @Autowired
    DecisionDao decisionDao;

    @RequestMapping("/saveNewDecision")
    public ModelAndView saveNewDecision(Decision decision) {
        final int decisionId;
        final Integer maxId = decisionDao.findMaxId();
        if (maxId == null) {
            decisionId = 0;
        } else {
            decisionId = maxId + 1;
        }
        decision.setId(decisionId);

        decisionDao.save(decision);

        ModelAndView mv = new ModelAndView("decision");
        mv.addObject("decision", decision);
        return mv;
    }

    @RequestMapping("/openDecision")
    public ModelAndView decision(@RequestParam Integer decisionId) {
        Optional<Decision> decisionOptional = decisionDao.findById(decisionId);
        if (decisionOptional.isEmpty()) {
            throw new IllegalArgumentException(); //todo error page schreiben
        }

        ModelAndView mv = new ModelAndView("decision");
        mv.addObject("decision", decisionOptional.get());
        return mv;
    }

    @RequestMapping("/saveDecision")
    public ModelAndView saveDecision(Decision decision) {
        decisionDao.save(decision);
        return decision(decision.getId());
    }
}
