package an.deciderweb;

public class Constants {
    public static final boolean DEVELOPER_EXCEPTIONS = true;
    public static final int MIN_UTILITY = 0;
    public static final int MAX_UTILITY = 10;
    public static final int DEFAULT_UTILITY = 5;
    public static final int NO_WAY_UTILITY = -1;

}
