package an.deciderweb.dao;

import an.deciderweb.model.Decision;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface DecisionDao extends CrudRepository<Decision, Integer> {

    @Query(value = "SELECT MAX(Id) FROM Decision", nativeQuery = true)
    Integer findMaxId();
}
