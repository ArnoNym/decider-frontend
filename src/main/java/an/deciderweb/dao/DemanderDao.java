package an.deciderweb.dao;

import an.deciderweb.model.Demander;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DemanderDao extends CrudRepository<Demander, Integer> {
    List<Demander> findAllByDecisionId(Integer decisionId);

    @Query(value = "SELECT MAX(Id) FROM Demander", nativeQuery = true)
    Integer findMaxId();

    @Query(value = "SELECT Id FROM Demander", nativeQuery = true)
    List<Integer> findAllId();
}
