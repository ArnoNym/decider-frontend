package an.deciderweb.dao;

import an.deciderweb.model.Offer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface OfferDao extends CrudRepository<Offer, Integer> {
    List<Offer> findAllByDecisionId(Integer decisionId);

    @Query(
        value = "SELECT Id " +
                "FROM Offer " +
                "WHERE Id" +
                "NOT IN (SELECT offer_Id FROM Opinion WHERE demander_Id = ?1)",
        nativeQuery = true
    )
    List<Integer> findAllOfferIdsNotInOpinionForDemander(@RequestParam("demanderId") Integer demanderId);

    @Query(value = "SELECT MAX(Id) FROM Offer", nativeQuery = true)
    Integer findMaxId();
}
