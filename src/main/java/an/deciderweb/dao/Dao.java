package an.deciderweb.dao;

import an.deciderweb.model.Model;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

public interface Dao<T extends Model<Integer>> {
    @Query(value = "SELECT MAX(Id) FROM Decision", nativeQuery = true)
    Integer findMaxId();
}
