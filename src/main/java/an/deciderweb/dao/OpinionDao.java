package an.deciderweb.dao;

import an.deciderweb.DeciderwebApplication;
import an.deciderweb.model.Opinion;
import an.deciderweb.model.id.OpinionId;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.TypedQuery;
import java.util.LinkedList;
import java.util.List;

public interface OpinionDao extends CrudRepository<Opinion, OpinionId> {
    //TODO Funktioniert leider nicht, da er das nicht zu Opinion casten kann :(

    /*default List<Opinion> findAllByDecisionId(Integer decisionId) {
        TypedQuery<Opinion> typedQuery = DeciderwebApplication.getEntityManager().createQuery(
                "SELECT o.id.demanderId, o.id.offerId, o.value, o.note " +
                "FROM Opinion AS o, Demander AS d " +
                "WHERE o.id.demanderId = d.id AND d.decisionId = :decisionId",
                Opinion.class);
        typedQuery.setParameter("decisionId", decisionId);
        return typedQuery.getResultList();
    }*/

    @Query(
            value = "SELECT o.demander_Id, o.offer_Id, o.value, o.note " +
                    "FROM Opinion AS o, Demander AS d " +
                    "WHERE o.demander_Id = d.id AND d.decision_Id = ?1",
            nativeQuery = true
    )
    List<Opinion> findAllByDecisionId(@RequestParam("decisionId") Integer decisionId);

    @Query(
            value = "SELECT o.demander_Id, o.offer_Id, o.value, o.note " +
                    "FROM Opinion AS o, Demander AS d " +
                    "WHERE o.demander_Id = d.id " +
                    "AND d.decision_Id = ?1 " +
                    "AND o.demander_Id = ?2",
            nativeQuery = true
    )
    List<Opinion> findAllByDecisionIdAAndDemanderId(@RequestParam("decisionId") Integer decisionId, @RequestParam("demanderId") Integer demanderId);

    /*
    @Query("SELECT o.id.demanderId, o.id.offerId, o.value, o.note FROM Opinion AS o, Demander AS d WHERE o.id.demanderId = d.id AND d.decisionId = ?1")
    List<Opinion> findAllByDecisionId(@RequestParam("decisionId") Integer decisionId);
    */



}
