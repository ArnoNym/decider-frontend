CREATE TABLE Test (
    Id INT NOT NULL,
	Name VARCHAR(50),

    PRIMARY KEY(Id)
);

CREATE TABLE Decision (
    Id INT NOT NULL,
	Name VARCHAR(50) NOT NULL,
    Password VARCHAR(50),
    Min_Group_Count INT NOT NULL,
    Max_Group_Count INT,
    Note VARCHAR(500),

    PRIMARY KEY(Id)
);

CREATE TABLE Demander (
    Id INT NOT NULL,
	Decision_Id INT NOT NULL,
	Name VARCHAR(50) NOT NULL,
    Has_To_Be_Included BOOLEAN NOT NULL,

    PRIMARY KEY(Id),
    FOREIGN KEY(Decision_Id) REFERENCES Decision(Id) ON DELETE CASCADE
);

CREATE TABLE Offer (
    Id INT NOT NULL,
	Decision_Id INT NOT NULL,
	Name VARCHAR(50) NOT NULL,
    Has_To_Be_Included BOOLEAN NOT NULL,
    Available INT NOT NULL,
    Min_Group_Size INT NOT NULL,
    Max_Group_Size INT,
    Note VARCHAR(500),

    PRIMARY KEY(Id),
    FOREIGN KEY(Decision_Id) REFERENCES Decision(Id) ON DELETE CASCADE
);

CREATE TABLE Opinion (
	Demander_Id INT NOT NULL,
	Offer_Id INT NOT NULL,
    Value DOUBLE,
    Note VARCHAR(500),

    PRIMARY KEY(Demander_Id, Offer_Id),
    FOREIGN KEY(Demander_Id) REFERENCES Demander(Id) ON DELETE CASCADE,
    FOREIGN KEY(Offer_Id) REFERENCES Offer(Id) ON DELETE CASCADE
);