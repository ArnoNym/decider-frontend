

INSERT INTO Decision (id, name, password, min_Group_Count, max_Group_Count, note) VALUES
(11, 'BrettspielAG Essen', NULL, 1, NULL, NULL),
(12, 'BrettspielAG Spielen', NULL, 1, NULL, NULL);

INSERT INTO Demander (id, decision_id, name, has_To_Be_Included) VALUES
(11, 11, 'Tom', FALSE),
(12, 11, 'Tim', FALSE),
(13, 11, 'Rolph', FALSE),
(14, 11, 'Martin', FALSE);

INSERT INTO Offer (Id, Decision_Id, Name, Has_To_Be_Included, Available, Min_Group_Size, Max_Group_Size, Note) VALUES
(11, 11, 'Schach', FALSE, 1, 1, null, ''),
(12, 11, 'Risiko', FALSE, 1, 1, null, ''),
(13, 11, 'Carcassone', FALSE, 1, 1, null, '');

INSERT INTO Opinion (Demander_Id, Offer_Id, Value, Note) VALUES
(11, 11, 5, ''),
(11, 12, 7, ''),
(11, 13, 7, ''),
(12, 11, 9, ''),
(12, 12, 4, ''),
(12, 13, 4, ''),
(13, 11, 5, ''),
(13, 12, 7, ''),
(13, 13, 7, ''),
(14, 11, 5, ''),
(14, 12, 7, ''),
(14, 13, 7, '');


/*
INSERT INTO Decision VALUES (11, 'BrettspielAG Essen', NULL , 1, 2, NULL);

INSERT INTO Decision VALUES (11, 'BrettspielAG Essen', NULL , 1, 2, NULL);

INSERT INTO Decision (id, name, password, min_Group_Count, max_Group_Count, note) VALUES
(11, 'BrettspielAG Essen', NULL , 1, 2, NULL),
(12, 'BrettspielAG Essen', NULL , 1, 2, NULL);

INSERT INTO Decision VALUES ('BrettspielAG Essen', NULL, 1, 1234);
INSERT INTO Decision VALUES ('BrettspielAG Spiele', 3, 1, 1234);

INSERT INTO Demander VALUES (1, TRUE, 'Peter');
INSERT INTO Demander VALUES (2, TRUE, 'Hans');

INSERT INTO Offer VALUES (1, 1, 5, 2, 'Risiko', '');
INSERT INTO Offer VALUES (2, 1, 2, 2, 'Schach', '');
*/

/*
INSERT INTO Opinion (DemanderName, OfferName, Opinion)
VALUES ('Peter', 'Risiko', 8);
INSERT INTO Opinion (DemanderName, OfferName, Opinion)
VALUES ('Peter', 'Schach', 7);

INSERT INTO Opinion (DemanderName, OfferName, Opinion)
VALUES ('Hans', 'Risiko', 4);
INSERT INTO Opinion (DemanderName, OfferName, Opinion)
VALUES ('Hans', 'Schach', 3);
*/